USE musikvoting;
CREATE TABLE IF NOT EXISTS T_lieder (
  P_bandname varchar(200) NOT NULL,
  genre varchar(200) NOT NULL,
  P_titelname varchar(200) NOT NULL,
  primary key (P_bandname, P_titelname)
);
USE musikvoting;
CREATE TABLE IF NOT EXISTS T_user (
    P_username varchar(200) NOT NULL,
    primary key (P_username)
);
USE musikvoting;
CREATE TABLE IF NOT EXISTS T_voting(
    PF_username varchar(200),
    PF_bandname varchar(200),
    PF_titelname varchar(200),
    P_voteid INT AUTO_INCREMENT,
    VF boolean not null default 0,
    primary key (P_voteid, PF_username, PF_bandname, PF_titelname),
    FOREIGN KEY (PF_username) REFERENCES T_user (P_username),
    FOREIGN KEY (PF_bandname, PF_titelname) REFERENCES T_lieder (P_bandname, P_titelname)
)