USE musikvoting;
Call musikvoting.vote_proc();
SELECT PF_bandname, PF_titelname, genre, COUNT(*) AS anz_votes FROM 
(SELECT PF_bandname, PF_titelname, PF_username FROM t_voting WHERE vf=1) as a, 
(SELECT genre, P_bandname, P_titelname FROM t_lieder) as b 
WHERE a.PF_bandname = b.P_bandname and a.PF_titelname=b.P_titelname GROUP by PF_titelname ORDER by anz_votes desc;
