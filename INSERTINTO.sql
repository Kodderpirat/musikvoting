USE musikvoting;
INSERT INTO T_user (P_username)
VALUES 
    ('Bob'),
    ('Bert'),
    ('Ingo'),
    ('Bertos'),
    ('Karlos'),
    ('Berta'),
    ('Bertosso');

USE musikvoting;
INSERT INTO T_lieder (P_titelname, P_bandname, genre)
VALUES 
('Raise your Horns', 'Amon Amarth', 'Metal'),
('Wide Awake', 'Betraying the Martyrs', 'Melodic Deathcore'),
('Black Flame', 'Bury Tomorrow', 'Metal'),
('Elective Amnesia', 'Rise Against', 'Rock'),
('Across The Rainbow Bridge', 'Amon Amarth', 'Metal'),
('We Drink your Blood', 'Powerwolf', 'Powermetal'),
('Forgiveness is Weakness', 'Whitechapel', 'Metal');

USE musikvoting; 
INSERT INTO T_voting (PF_username, PF_titelname, PF_bandname)
VALUES 
    ('Bob', 'Wide Awake', 'Betraying the Martyrs'),
    ('Bert', 'Raise your Horns', 'Amon Amarth'),
    ('Bob', 'Across the Rainbow Bridge', 'Amon Amarth'),
    ('Ingo', 'Black Flame', 'Bury Tomorrow'),
    ('Bertosso', 'Wide Awake', 'Betraying the Martyrs'),
    ('Berta', 'We Drink your Blood', 'Powerwolf'),
    ('Karlos', 'Across the Rainbow Bridge', 'Amon Amarth'),
    ('Bertosso', 'Black Flame', 'Bury Tomorrow'),
    ('Bob', 'Raise your Horns', 'Amon Amarth'),
    ('Ingo', 'Wide Awake', 'Betraying the Martyrs'),
    ('Bob','Forgiveness is Weakness', 'Whitechapel'),
    ('Ingo', 'Forgiveness is Weakness', 'Whitechapel'),
    ('Bertos', 'Forgiveness is Weakness', 'Whitechapel'),
    ('Berta', 'Forgiveness is Weakness', 'Whitechapel'),
    ('Karlos', 'Forgiveness is Weakness', 'Whitechapel'),
    ('Bert', 'Forgiveness is Weakness', 'Whitechapel'),
    ('Karlos', 'Wide Awake', 'Betraying the Martyrs'),
    ('Bob', 'Wide Awake', 'Betraying the Martyrs'),
    ('Karlos', 'Wide Awake', 'Betraying the Martyrs'),
    ('Ingo', 'Wide Awake', 'Betraying the Martyrs'),
    ('Bob', 'Across The Rainbow Bridge', 'Amon Amarth'),
    ('Ingo', 'Across The Rainbow Bridge', 'Amon Amarth'),
    ('Berta', 'Across The Rainbow Bridge', 'Amon Amarth'),
    ('Bert', 'Across The Rainbow Bridge', 'Amon Amarth');
