USE musikvoting;
create procedure vote_proc()

begin
declare cuser varchar(200);
declare finished int default 0;
declare user_cursor cursor for select P_username from T_user;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

open user_cursor;

userloop: loop

fetch user_cursor into cuser;

IF finished = 1 THEN LEAVE userloop; END IF;

CALL user_votes(cuser);

end loop userloop;

close user_cursor;

end