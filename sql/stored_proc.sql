create procedure user_votes (vuser varchar(200))

begin
declare v_vote int;
declare finished int default 0;
declare user_vote_cursor cursor for 
      select max(P_voteid) from T_voting where PF_username=vuser
      group by PF_bandname,PF_titelname order by P_voteid desc limit 5;
      
DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
      
update T_voting set VF=0 where PF_username=vuser;

open user_vote_cursor;

set_vote: loop
   
fetch user_vote_cursor into v_vote;

IF finished = 1 THEN LEAVE set_vote; END IF;

update T_voting set VF=1 where v_vote=P_voteid;

end loop set_vote;
close user_vote_cursor;

end